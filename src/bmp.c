#include "../include/bmp.h"
#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BMP_HEADER_SIZE 40
#define BITS_PER_PIXEL 24
#define BYTES_PER_PIXEL (BITS_PER_PIXEL / 8)
#include "../include/bmp.h"
#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BMP_FILE_TYPE 0x4D42
#define BMP_FILE_RESERVED 0
#define BMP_FILE_SIZE 40
#define BMP_FILE_PLANES 1
#define BMP_FILE_BITCOUNT 24
#define BMP_FILE_COMPRESSION 0
#define BMP_FILE_X_PELS_PER_METER 0
#define BMP_FILE_Y_PELS_PER_METER 0
#define BMP_FILE_CLR_USED 0
#define BMP_FILE_CLR_IMPORTANT 0


typedef struct bmp_header BMPHeader;


int calculate_padding(int width) {
    return (BMP_PADDING - (width * BYTES_PER_PIXEL) % BMP_PADDING) % BMP_PADDING;
}


enum read_status from_bmp(const char* filepath, struct image* img) {
    FILE* file = fopen(filepath, "rb");
    if (!file) {
        return READ_ERROR;
    }

    struct bmp_header header;
    fread(&header, sizeof(BMPHeader), 1, file);

    if (header.bfType != 0x4d42) {
        fclose(file);
        return READ_INVALID_SIGNATURE;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = (struct pixel*) malloc(img->width * img->height * sizeof(struct pixel));
    if (!img->data) {
        fclose(file);
        return READ_INVALID_BITS;
    }
    for (int y = img->height - 1; y >= 0; y--) {
        for (int x = 0; x < img->width; x++) {
            struct pixel p;
            fread(&p.b, sizeof(uint8_t), 1, file);
            fread(&p.g, sizeof(uint8_t), 1, file);
            fread(&p.r, sizeof(uint8_t), 1, file);
            img->data[y * img->width + x] = p;
        }
        for (int padding = 0; padding < calculate_padding(imgWidth); padding++) {
            fgetc(file);
        }
    }

    fclose(file);
    return READ_OK;
}

enum write_status to_bmp(const char* filepath, struct image const* img) {
    FILE* file = fopen(filepath, "wb");
    if (!file) {
        return WRITE_ERROR;
    }

    struct bmp_header header = {
            .bfType = BMP_FILE_TYPE,
            .bfileSize = BMP_HEADER_SIZE + (imgWidth * BYTES_PER_PIXEL + calculate_padding(imgWidth)) * imgHeight,
            .bfReserved = BMP_FILE_RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_FILE_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BMP_FILE_PLANES,
            .biBitCount = BMP_FILE_BITCOUNT,
            .biCompression = BMP_FILE_COMPRESSION,
            .biSizeImage = (imgWidth * BYTES_PER_PIXEL + calculate_padding(imgWidth)) * imgHeight,
            .biXPelsPerMeter = BMP_FILE_X_PELS_PER_METER,
            .biYPelsPerMeter = BMP_FILE_Y_PELS_PER_METER,
            .biClrUsed = BMP_FILE_CLR_USED,
            .biClrImportant = BMP_FILE_CLR_IMPORTANT
    };

    fwrite(&header, sizeof(struct bmp_header), 1, file);

    for (int y = img->height - 1; y >= 0; y--) {
        for (int x = 0; x < img->width; x++) {
            struct pixel p = img->data[y * img->width + x];
            fputc(p.b, file);
            fputc(p.g, file);
            fputc(p.r, file);  
        }
        for (int padding = 0; padding < calculate_padding(imgWidth); padding++) {
            fputc(0x00, file);
        }
    }
    fclose(file);
    return WRITE_OK;
}



