#include "../include/image.h"
#include "../include/bmp.h"
#include "../include/rotate.h"
#include "../include/sepia.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


int main(int argc, char* argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Использование: %s <input_file.bmp> <output_c.bmp> <output_asm.bmp>\n", argv[0]);
        return 1;
    }

    char* filepath = argv[1];
    char* c_savepath = argv[2];
    char* asm_savepath = argv[3];

    struct image source_image;
    if (from_bmp(filepath, &source_image) != READ_OK) {
        fprintf(stderr, "При чтении изображения произошла ошибка\n");
        return 1;
    }

    struct image c_sepia_image = source_image;
    struct image asm_sepia_image = source_image;

    clock_t start_c = clock();
    apply_sepia(c_sepia_image.data, c_sepia_image.width, c_sepia_image.height);
    clock_t end_c = clock();
    double cpu_time_used_c = ((double)(end_c - start_c)) / CLOCKS_PER_SEC;
    printf("Время выполнения сепии на C: %f секунд\n", cpu_time_used_c);

    clock_t start_asm = clock();
    apply_sepia_asm(&asm_sepia_image);
    clock_t end_asm = clock();
    double cpu_time_used_asm = ((double)(end_asm - start_asm)) / CLOCKS_PER_SEC;
    printf("Время выполнения сепии на ассемблере: %f секунд\n", cpu_time_used_asm);

    if (to_bmp(c_savepath, &c_sepia_image) != WRITE_OK) {
        fprintf(stderr, "Ошибка при сохранении изображения с сепией на C.\n");
        return 1;
    }

    if (to_bmp(asm_savepath, &asm_sepia_image) != WRITE_OK) {
        fprintf(stderr, "Ошибка при сохранении изображения с сепией на ассемблере.\n");
        return 1;
    }
    return 0;
}

