#include "../include/sepia.h"
#include "../include/image.h"
#include <stddef.h>


extern void apply_sepia_pix(struct pixel* pixel);

void apply_sepia(struct pixel *image, int width, int height) {
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int index = y * width + x;

            unsigned char r = image[index].r;
            unsigned char g = image[index].g;
            unsigned char b = image[index].b;

            int tr = (int)(0.393 * r + 0.769 * g + 0.189 * b);
            int tg = (int)(0.349 * r + 0.686 * g + 0.168 * b);
            int tb = (int)(0.272 * r + 0.534 * g + 0.131 * b);
            image[index].r = tr > 255 ? 255 : tr;
            image[index].g = tg > 255 ? 255 : tg;
            image[index].b = (tb + 20) > 255 ? 255 : (tb + 20);
        }
    }
}

void apply_sepia_asm(struct image* image) {
    if (image->data == NULL) {
        return;
    }

    uint64_t width = image->width;
    uint64_t height = image->height;

    for (uint64_t i = 0; i < height; i++) {
        for (uint64_t j = 0; j < width; j++) {
            apply_sepia_pix(image->data + (width * i + j));
        }
    }
}
