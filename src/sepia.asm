%macro to_float 2
    movzx esi, byte [%1]
    cvtsi2ss %2, esi
    shufps %2, %2, 0
%endmacro

section .rodata

blue_coefficients: dd 0.145, 0.172, 0.193, 0
green_coefficients: dd 0.567, 0.699, 0.771, 0
red_coefficients: dd 0.282, 0.355, 0.400, 0

section .text
global apply_sepia_pix

apply_sepia_pix:
    mov r10, [rdi + 3]
    transform_and_convert rdi, xmm3
    transform_and_convert rdi + 1, xmm4
    transform_and_convert rdi + 2, xmm5
    movups xmm6, [blue_coefficients]
    movups xmm7, [green_coefficients]
    movups xmm8, [red_coefficients]
    mulps xmm3, xmm6
    mulps xmm4, xmm7
    mulps xmm5, xmm8
    addps xmm3, xmm4
    addps xmm3, xmm5
    cvtps2dq xmm3, xmm3
    packusdw xmm3, xmm3
    packuswb xmm3, xmm3
    movd [rdi], xmm3
    mov [rdi + 3], r10
    ret

