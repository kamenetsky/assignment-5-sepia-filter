#include "../include/rotate.h"
#include <stdlib.h>

struct image rotate(struct image const* source, int angle) {
    struct image result;
    if (angle % 180 != 0) {
        result.width = source->height;
        result.height = source->width;
    } else {
        result.width = source->width;
        result.height = source->height;
    }
    result.data = calloc(result.width * result.height, sizeof(struct pixel));
    if (!result.data) {
        return (struct image){.width = 0, .height = 0, .data = NULL};
    }

    uint64_t src_width = source->width;
    uint64_t src_height = source->height;

    for (uint64_t y = 0; y < src_height; ++y) {
        for (uint64_t x = 0; x < src_width; ++x) {
            uint64_t src_index = y * src_width + x;
            uint64_t dst_x, dst_y;
            if (angle == 90 || angle == -270) {
                dst_x = src_height - 1 - y;
                dst_y = x;
            } else if (angle == 180 || angle == -180) {
                dst_x = src_width - 1 - x;
                dst_y = src_height - 1 - y;
            } else if (angle == 270 || angle == -90) {
                dst_x = y;
                dst_y = src_width - 1 - x;
            } else {
                dst_x = x;
                dst_y = y;
            }

            uint64_t dst_index = dst_y * result.width + dst_x;
            result.data[dst_index] = source->data[src_index];
        }
    }

    return result;
}

