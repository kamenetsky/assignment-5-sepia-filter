#ifndef SEPIA_H
#define SEPIA_H

#include "image.h"


void apply_sepia(struct pixel *image, int width, int height);
void apply_sepia_asm(struct image* image);

#endif

